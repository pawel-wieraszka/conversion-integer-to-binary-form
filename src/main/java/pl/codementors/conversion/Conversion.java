package pl.codementors.conversion;

import java.util.Scanner;

/**
 * Converts an integer from 0 to 15 into a binary form.
 *
 * @author Paweł Wieraszka
 */
public class Conversion {
    public static void main(String[] args){

        int[] score = new int[4];
        boolean running = true;
        Scanner input = new Scanner(System.in);

        while (running) {
            System.out.println("1 - integer to binary form"+
                               "\n0 - exit");
            int command = input.nextInt();

            switch (command) {
                case 1: {
                    System.out.println("Enter an integer from 0 to 15:");
                    int number = input.nextInt();

                    if (number >= 0 && number < 16) {
                        System.out.print("Your integer: "+number+" in binary form is: ");
                        for (int i = 3; i >= 0; i--) {
                            score[i]= number % 2;
                            number /= 2;
                        }
                        for (int s : score) {
                            System.out.print(s);
                        }
                        System.out.println("\n");
                    }
                    else {
                        System.out.println("You entered a wrong number!\n");
                    }
                    break;
                }
                case 0: {
                    running = false;
                    break;
                }
            }
        }
    }
}
